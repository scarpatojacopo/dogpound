
/****************************************************************
 Dog.java

 A class that holds a dog's name and can make it speak.

****************************************************************/
public class Dog
{
    protected String name;
    protected  String color;

    /**
     * Constructor - stores name
     * @param name
     */

    public Dog(String name)
    {
        this.name = name;
    }

    public Dog(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Dog() {
    }

    /**
     * Getter
     * @return dog's name
     */

    public String getName()
    {
        return name;
    }

        /**
         * Barking method
         * @return a string of the dog's comment on life
         */
        public String speak ()
        {
            return "Woof";
        }


    }